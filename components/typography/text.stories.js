import React from "react";
import { Text } from "./text";

export default {
  title: "Text",
  component: Text,
};

export const Example = () => <Text />;
