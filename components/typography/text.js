import React from "react";
import { ColorBaseGrayMedium } from "../tokens";

export const Text = () => {
  return (
    <div>
      <h1 style={{ color: ColorBaseGrayMedium }}>Text component</h1>
    </div>
  );
};
